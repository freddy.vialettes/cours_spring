package fr.vialettes.freddy.spring;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.vialettes.freddy.spring.feature.database.product.ProductAdd;
import jakarta.transaction.Transactional;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
class ApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	private ProductAdd productAdd;

	@Test
	void contextLoads() {
	}

	@Test
	void testHelloWordApiEndPoint() throws Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/hello/world").contentType(MediaType.TEXT_PLAIN_VALUE);
		ResultMatcher resultStatus = MockMvcResultMatchers.status().isOk();
		ResultMatcher resultContent = MockMvcResultMatchers.content().string("Hello World !");
		mockMvc.perform(requestBuilder)
			.andExpect(resultStatus)
			.andExpect(resultContent);
	}

	@Test
	@Transactional
	void testAddProduct() throws Exception {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("nom", "Ordinateur 2");
		jsonObject.put("description", "test");
		jsonObject.put("prix", 12);
		jsonObject.put("categorieId", 2);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/product").contentType(MediaType.APPLICATION_JSON).content(String.valueOf(jsonObject));
		ResultMatcher resultStatus = MockMvcResultMatchers.status().isOk();
		mockMvc.perform(requestBuilder)
				.andExpect(resultStatus);
	}

	@Test
	void testGetProduct() throws Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/product");
		ResultMatcher resultStatus = MockMvcResultMatchers.status().isOk();

		mockMvc.perform(requestBuilder).andExpect(resultStatus);
	}

	@Test
	void testUpdateProduct() throws Exception {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("nom", "Ordinateur blah");
		jsonObject.put("description", "blahblah");
		jsonObject.put("prix", 12);
		jsonObject.put("categorieId", 2);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.put("/api/product/update/5")
				.contentType(MediaType.APPLICATION_JSON)
				.content(String.valueOf(jsonObject));
		ResultMatcher resultStatus = MockMvcResultMatchers.status().isOk();

		mockMvc.perform(requestBuilder).andExpect(resultStatus);
	}

}
