package fr.vialettes.freddy.spring.database;

import fr.vialettes.freddy.spring.feature.database.DatabaseService;
import fr.vialettes.freddy.spring.feature.database.avis.Avis;
import fr.vialettes.freddy.spring.feature.database.avis.AvisDto;
import fr.vialettes.freddy.spring.feature.database.category.Category;
import fr.vialettes.freddy.spring.feature.database.category.CategoryDto;
import fr.vialettes.freddy.spring.feature.database.clients.Clients;
import fr.vialettes.freddy.spring.feature.database.clients.ClientsDto;
import fr.vialettes.freddy.spring.feature.database.product.Product;
import fr.vialettes.freddy.spring.feature.database.product.ProductDto;
import fr.vialettes.freddy.spring.feature.database.product.ProductRepository;
import fr.vialettes.freddy.spring.feature.database.product.ProductWithPrice;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.assertj.core.condition.Negative;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.swing.*;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class DatabaseTests {

    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private DatabaseService databaseService;

    private Logger LOGGER = LoggerFactory.getLogger(DatabaseTests.class);

    @Test
    void testGetProductNameList() {
        List<String> result = databaseService.getProductNameList();

        assertNotNull(result);
        assertEquals(result.get(0), "Filet de poulet");
    }

    @Test
    void testGetProductList() {
        ProductDto p1 = new ProductDto(1, "Filet de poulet", "Filet de poulet 1kg");
        ProductDto p2 = new ProductDto(2, "Côte de boeuf", "Côte de boeuf maturée 1,5kg");
        List<ProductDto> result = databaseService.getProductList();

        assertNotNull(result);
        assert result.containsAll(Arrays.asList(p1, p2));
    }

    @Test
    void testGetProductListPrices() {

        Map<Integer, BigDecimal> listPrices = new HashMap<>();
        listPrices.put(1, BigDecimal.valueOf(10.0));

        List<ProductWithPrice> result = databaseService.getProductListWithPrices();

        assert result.stream()
                .filter(produit -> listPrices.containsKey(produit.getId()))
                .allMatch(produit ->
                        produit.getPrix().equals(listPrices.get(produit.getId()))
                );
    }

    boolean testEquality(Product pProduct, ProductWithPrice pProductWithPrice) {
        return Objects.equals(pProduct.getNom(), pProductWithPrice.getNom()) && Objects.equals(pProduct.getDescription(), pProductWithPrice.getDescription())
                && Objects.equals(BigDecimal.valueOf(pProduct.getPrix()), pProductWithPrice.getPrix());
    }

    @Test
    void testProduitFromEntity() {
        ProductWithPrice p1 = new ProductWithPrice(1, "Filet de poulet", "Filet de poulet 1kg", BigDecimal.valueOf(10.0));
        ProductWithPrice p2 = new ProductWithPrice(2, "Côte de boeuf", "Côte de boeuf maturée 1,5kg", BigDecimal.valueOf(30.0));
        ProductWithPrice p3 = new ProductWithPrice(3, "PS5", "PS5 1To", BigDecimal.valueOf(350.0));
        ProductWithPrice p4 = new ProductWithPrice(4, "Iphone", "Iphone noir 64Go", BigDecimal.valueOf(250.0));

        List<Product> listProductWithEntity = databaseService.getListProductWithEntity();

        assert listProductWithEntity.stream().allMatch(produit -> testEquality(produit, p1) || testEquality(produit, p2) || testEquality(produit, p3) || testEquality(produit, p4));
    }

    @Test
    void testCategoryFromEntity() {
        CategoryDto c1 = new CategoryDto(1, "nourriture");
        CategoryDto c2 = new CategoryDto(2, "Multimédia");

        List<Category> listCategoryWithEntity = databaseService.getListCatagoryWithEntity();

        assert listCategoryWithEntity.stream().allMatch(category -> category.getNom().equals(c1.getNom()) || category.getNom().equals(c2.getNom()));

    }

    @Test
    void testAvisFromEntity() {
        AvisDto a1 = new AvisDto(2, "Le filet de poulet est super bon", 10, 1, 1);
        AvisDto a2 = new AvisDto(3, "J'adore la PS5 !", 5, 3, 1);

        List<Avis> listAvisWithEntity = databaseService.getListAvisWithEntity();

        assert listAvisWithEntity.stream().allMatch(avis ->
                avis.getDescription().equals(a1.getDescription())
                || avis.getNote().equals(a1.getNote())
                || avis.getDescription().equals(a2.getDescription())
                || avis.getNote().equals(a2.getNote())
        );

    }

    @Test
    void testClientsFromEntity() {
        ClientsDto c1 = new ClientsDto(1, "Vialettes", "Freddy", "test@gmail.com");

        List<Clients> listClientsWithEntity = databaseService.getListClientsWithEntity();

        assert listClientsWithEntity.stream().allMatch(client -> client.getNom().equals(c1.getNom()) || client.getPrenom().equals(c1.getPrenom()) || client.getEmail().equals(c1.getEmail()));
    }

    @Test
    void testProductByName() {
        ProductWithPrice p1 = new ProductWithPrice(4, "Iphone", "Iphone noir 64Go", BigDecimal.valueOf(250.0));
        ProductWithPrice p2 = new ProductWithPrice(5, "Iphone 13", "Iphone 13 gris 128Go", BigDecimal.valueOf(1200.0));

        List<Product> listProduct = databaseService.getProductByName("Iphone");

        assert listProduct.stream().allMatch(produit -> testEquality(produit, p1) || testEquality(produit, p2));
    }

    @Test
    void testProductByCategoryName() {
        ProductWithPrice p1 = new ProductWithPrice(4, "Iphone", "Iphone noir 64Go", BigDecimal.valueOf(250.0));
        ProductWithPrice p2 = new ProductWithPrice(5, "Iphone 13", "Iphone 13 gris 128Go", BigDecimal.valueOf(1200.0));
        ProductWithPrice p3 = new ProductWithPrice(3, "PS5", "PS5 1To", BigDecimal.valueOf(350.0));
        ProductWithPrice p4 = new ProductWithPrice(2, "Ordinateur", "test", BigDecimal.valueOf(12.0));

        List<Product> productList = databaseService.getProductByCategoryName("Multimédia");
        for (Product product : productList) {
            LOGGER.info("Le produit {} fait parti de la categorie Multimédia", product.getNom());
        }

        assert productList.stream().allMatch(produit -> testEquality(produit, p1) || testEquality(produit, p2) || testEquality(produit, p3) || testEquality(produit, p4));
    }
}
