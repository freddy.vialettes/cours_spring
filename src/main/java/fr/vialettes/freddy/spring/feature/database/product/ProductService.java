package fr.vialettes.freddy.spring.feature.database.product;

import jakarta.persistence.EntityNotFoundException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MissingRequestValueException;

import java.sql.SQLException;
import java.util.List;
import java.util.MissingResourceException;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public Product insertNewProduct(Product pProduct) throws IllegalArgumentException, MissingRequestValueException {
//        List<Product> productList = productRepository.getProductByNomContaining(pProduct.getNom());
//        if (productList.size() > 0 ) {
//            throw new IllegalArgumentException("Name already exist");
//        }
//
//        if (pProduct.getDescription() == null | pProduct.getCategorieId() == null) {
//            throw new MissingRequestValueException("Required fiedls is missing ! ");
//        }
//
//        if(pProduct.getNom() == null) {
//            throw new MissingResourceException("Empty or invalid", "Product", "name");
//        }
//
//        if(pProduct.getPrix() == null) {
//            throw new MissingResourceException("Field prix is required", "Product", "prix");
//        }
        return productRepository.save(pProduct);
    }

    public List<Product> getAllProduct() {
        return productRepository.findAll();
    }

    public Product updateProduct(Product pProduct) {
        return productRepository.save(pProduct);
    }

    public Optional<Product> getProductById(Integer pId) {
        return productRepository.findById(pId);
    }

    public void deleteProduct(Integer pProductId) throws EntityNotFoundException{
        Optional<Product> productById = productRepository.findById(pProductId);

        if(productById.isEmpty()) {
            throw new EntityNotFoundException("Produit with Id %d not exist".formatted(pProductId));
        }

        productRepository.deleteById(pProductId);
    }

    public Product updateProduct(Integer productId, Product pProduct) throws EntityNotFoundException, IllegalArgumentException{
        Optional<Product> productById = productRepository.findById(productId);

        if (productById.isEmpty()) {
            throw new EntityNotFoundException("Produit with Id %d not exist".formatted(productId));
        }

        List<Product> productList = productRepository.getProductByNomContaining(pProduct.getNom());
        if (productList.size() > 0 ) {
            throw new IllegalArgumentException("Name for product : " + "'" +  pProduct.getNom() + "'" + " already exist");
        }

        Product product = productById.get();
        if(pProduct.getNom() != null) {
            product.setNom(pProduct.getNom());
        }
        if(pProduct.getDescription() != null) {
            product.setDescription(pProduct.getDescription());
        }
        if(pProduct.getPrix() != null) {
            product.setPrix(pProduct.getPrix());
        }
        if(pProduct.getCategorieId() != null) {
            product.setCategorieId(pProduct.getCategorieId());
        }
        return productRepository.save(product);
    }
}
