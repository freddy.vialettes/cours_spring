package fr.vialettes.freddy.spring.feature.database.product;


import jakarta.persistence.*;

import java.util.Objects;
import java.util.Optional;


public class ProductDto {

    private Integer id;
    private String nom;
    private String description;
    private Integer categorie_id;
    public ProductDto(Integer pId, String pNom, String pDescription) {
        id = pId;
        nom = pNom;
        description = pDescription;
    }
    public ProductDto(Tuple pTuple) {
        this.id = (Integer) pTuple.get(0);
        this.nom = (String) pTuple.get(1);
        this.description = Optional.ofNullable((String) pTuple.get(2)).orElse("DEFAULT");
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer pId) {
        id = pId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String pNom) {
        nom = pNom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String pDescription) {
        description = pDescription;
    }

    public Integer getCategorie_id() {
        return categorie_id;
    }

    public void setCategorie_id(Integer pCategorie_id) {
        categorie_id = pCategorie_id;
    }

    @Override
    public boolean equals(Object pO) {
        if (this == pO) return true;
        if (pO == null || getClass() != pO.getClass()) return false;
        ProductDto product = (ProductDto) pO;
        return Objects.equals(id, product.id) && Objects.equals(nom, product.nom) && Objects.equals(description, product.description) && Objects.equals(categorie_id, product.categorie_id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom, description, categorie_id);
    }
}
