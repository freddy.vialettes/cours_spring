package fr.vialettes.freddy.spring.feature.database.clients;

import jakarta.persistence.*;

public class ClientsDto {

    private Integer id;
    private String nom;
    private String prenom;
    private String email;

    public ClientsDto(Integer pId, String pNom, String pPrenom, String pEmail) {
        id = pId;
        nom = pNom;
        prenom = pPrenom;
        email = pEmail;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer pId) {
        id = pId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String pNom) {
        nom = pNom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String pPrenom) {
        prenom = pPrenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String pEmail) {
        email = pEmail;
    }
}
