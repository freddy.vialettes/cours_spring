package fr.vialettes.freddy.spring.feature.database.avis;

import jakarta.persistence.*;

@Entity
@Table(name = "avis")
public class Avis {

    @Column(name="id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "description")
    private String description;

    @Column(name = "note")
    private Integer note;

    @Column(name = "produits_id")
    private Integer produits_id;

    @Column(name = "clients_id")
    private Integer clients_id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer pId) {
        id = pId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String pDescription) {
        description = pDescription;
    }

    public Integer getNote() {
        return note;
    }

    public void setNote(Integer pNote) {
        note = pNote;
    }

    public Integer getProduits_id() {
        return produits_id;
    }

    public void setProduits_id(Integer pProduits_id) {
        produits_id = pProduits_id;
    }

    public Integer getClients_id() {
        return clients_id;
    }

    public void setClients_id(Integer pClients_id) {
        clients_id = pClients_id;
    }
}
