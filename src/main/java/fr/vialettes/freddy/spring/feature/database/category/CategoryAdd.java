package fr.vialettes.freddy.spring.feature.database.category;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CategoryAdd {

    @JsonProperty(value = "nom")
    private String nom;

    public CategoryAdd(String pNom) {
        nom = pNom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String pNom) {
        nom = pNom;
    }
}
