package fr.vialettes.freddy.spring.feature.database.product;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.stereotype.Service;

public class ProductAdd {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("nom")
    private String nom;

    @JsonProperty("description")
    private String description;

    @JsonProperty("prix")
    private double prix;

    @JsonProperty("categorie_id")
    private Integer categorie_id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer pId) {
        id = pId;
    }

    public String getNom() {
        return nom;
    }

    public String getDescription() {
        return description;
    }

    public double getPrix() {
        return prix;
    }

    public Integer getCategorie_id() {
        return categorie_id;
    }

    public void setNom(String pNom) {
        nom = pNom;
    }

    public void setDescription(String pDescription) {
        description = pDescription;
    }

    public void setPrix(double pPrix) {
        prix = pPrix;
    }

    public void setCategorie_id(Integer pCategorie_id) {
        categorie_id = pCategorie_id;
    }
}
