package fr.vialettes.freddy.spring.feature.database.product;

import fr.vialettes.freddy.spring.feature.database.category.Category;
import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import org.aspectj.lang.annotation.RequiredTypes;

import java.util.Objects;

@Entity
@Table(name="produits")
public class Product {
    @Column(name="id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "nom")
    @NotEmpty(message = "name must not be empty")
    private String nom;

    @Column(name = "description")
    @NotEmpty(message = "description must not be empty")
    @Pattern(regexp = "[a-zA-Z]*")
    private String description;

    @Column(name = "prix")
    @NotNull(message = "prix must not be null")
    @Min(value = 0, message = "Le prix doit être supérieur ou égal à 0")
    private Double prix;

    @Column(name = "categorie_id")
    @NotNull(message = "categorie must not be null")
    private Integer categorieId;

    @JoinColumn(name = "categorie_id", updatable = false, insertable = false)
    @ManyToOne
    private Category categorie;

    public Product() {

    }

    public Integer getCategorieId() {
        return categorieId;
    }

    public void setCategorieId(Integer pCategorieId) {
        categorieId = pCategorieId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer pId) {
        id = pId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String pNom) {
        nom = pNom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String pDescription) {
        description = pDescription;
    }

    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double pPrix) {
        prix = pPrix;
    }

    @Override
    public boolean equals(Object pO) {
        if (this == pO) return true;
        if (pO == null || getClass() != pO.getClass()) return false;
        Product product = (Product) pO;
        return Objects.equals(id, product.id) && Objects.equals(nom, product.nom) && Objects.equals(description, product.description)
                && Objects.equals(prix, product.prix);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom, description);
    }
}
