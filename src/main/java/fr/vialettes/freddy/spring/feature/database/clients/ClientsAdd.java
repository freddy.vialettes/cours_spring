package fr.vialettes.freddy.spring.feature.database.clients;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ClientsAdd {

    @JsonProperty(value = "nom")
    private String nom;

    @JsonProperty(value = "prenom")
    private String prenom;

    @JsonProperty(value = "email")
    private String email;

    public ClientsAdd(String pNom, String pPrenom, String pEmail) {
        nom = pNom;
        prenom = pPrenom;
        email = pEmail;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String pNom) {
        nom = pNom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String pPrenom) {
        prenom = pPrenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String pEmail) {
        email = pEmail;
    }
}
