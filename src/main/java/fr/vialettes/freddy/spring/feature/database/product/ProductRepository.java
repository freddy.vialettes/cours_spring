package fr.vialettes.freddy.spring.feature.database.product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Integer> {

    List<Product> getProductByNomContaining(String nom);

    @Query("select p from Product p where p.categorie.nom = ?1")
    List<Product> getProductByCategoryName(String categoryName);

}
