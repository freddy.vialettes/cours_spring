package fr.vialettes.freddy.spring.feature.database.avis;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AvisRepository extends JpaRepository<Avis, Integer> {

}
