package fr.vialettes.freddy.spring.feature.database.avis;

import jakarta.persistence.*;


public class AvisDto {

    private Integer id;

    private String description;

    private Integer note;

    private Integer produits_id;

    private Integer clients_id;

    public AvisDto(Integer pId, String pDescription, Integer pNote, Integer pProduits_id, Integer pClients_id) {
        id = pId;
        description = pDescription;
        note = pNote;
        produits_id = pProduits_id;
        clients_id = pClients_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer pId) {
        id = pId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String pDescription) {
        description = pDescription;
    }

    public Integer getNote() {
        return note;
    }

    public void setNote(Integer pNote) {
        note = pNote;
    }

    public Integer getProduits_id() {
        return produits_id;
    }

    public void setProduits_id(Integer pProduits_id) {
        produits_id = pProduits_id;
    }

    public Integer getClients_id() {
        return clients_id;
    }

    public void setClients_id(Integer pClients_id) {
        clients_id = pClients_id;
    }
}
