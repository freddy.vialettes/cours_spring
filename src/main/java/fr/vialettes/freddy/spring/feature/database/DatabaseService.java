package fr.vialettes.freddy.spring.feature.database;

import fr.vialettes.freddy.spring.feature.database.avis.Avis;
import fr.vialettes.freddy.spring.feature.database.avis.AvisRepository;
import fr.vialettes.freddy.spring.feature.database.category.Category;
import fr.vialettes.freddy.spring.feature.database.category.CategoryRepository;
import fr.vialettes.freddy.spring.feature.database.clients.Clients;
import fr.vialettes.freddy.spring.feature.database.clients.ClientsRepository;
import fr.vialettes.freddy.spring.feature.database.product.Product;
import fr.vialettes.freddy.spring.feature.database.product.ProductDto;
import fr.vialettes.freddy.spring.feature.database.product.ProductRepository;
import fr.vialettes.freddy.spring.feature.database.product.ProductWithPrice;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;
import jakarta.persistence.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DatabaseService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseService.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private AvisRepository avisRepository;

    @Autowired
    private ClientsRepository clientsRepository;

    public List<String> getProductNameList() {
        List<String> result = new ArrayList<>();

        try {
            Query query = entityManager.createNativeQuery("SELECT nom FROM produits;", Tuple.class);
            List<Tuple> resultList = query.getResultList();

            result = resultList.stream().map(tuple -> (String) tuple.get(0)).collect(Collectors.toList());

//            for (Tuple result : results) {
//                String productName = (String) result.get("nom");
//                if(productName != null && !productName.isEmpty()) {
//                    productNameList.add(productName);
//                }
//            }

            LOGGER.info("Liste des noms de produits : {}", result);
        }
        catch (Exception e) {
            LOGGER.error("Erreur lors de la récupération de la liste des noms de produits : {}", e.getMessage(), e);
        }

        return result;
    }

    public List<ProductDto> getProductList() {
        List<ProductDto> productList = new ArrayList<>();

        try {
            Query query = entityManager.createNativeQuery("SELECT id, nom, description FROM produits;", Tuple.class);
            List<Tuple> resultList = query.getResultList();

            productList = resultList.stream().map(ProductDto::new).toList();
            LOGGER.info("Liste des produits : {}", productList);
        } catch (Exception e) {
            LOGGER.error("Erreur lors de la récupération de la liste des produits : {}", e.getMessage(), e);
        }

        return productList;
    }

    public List<ProductWithPrice> getProductListWithPrices() {
        List<ProductWithPrice> productList = new ArrayList<>();

        try {
            Query query = entityManager.createNativeQuery("SELECT id, nom, description, prix FROM produits;", Tuple.class);
            List<Tuple> resultList = query.getResultList();

            productList = resultList.stream().map(ProductWithPrice::new).toList();
            LOGGER.info("Liste des produits : {}", productList);
        } catch (Exception e) {
            LOGGER.error("Erreur lors de la récupération de la liste des produits : {}", e.getMessage(), e);
        }

        return productList;
    }

    public List<Product> getListProductWithEntity() {
        return productRepository.findAll();
    }
    public List<Category> getListCatagoryWithEntity() {
        return categoryRepository.findAll();
    }
    public List<Avis> getListAvisWithEntity() {
        return avisRepository.findAll();
    }
    public List<Clients> getListClientsWithEntity() {
        return clientsRepository.findAll();
    }

    public List<Product> getProductByName(String name) {
        return productRepository.getProductByNomContaining(name);
    }

    public List<Product> getProductByCategoryName(String categoryName) {
        return productRepository.getProductByCategoryName(categoryName);
    }

}
