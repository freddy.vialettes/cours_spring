package fr.vialettes.freddy.spring.feature.database.clients;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClientsRepository extends JpaRepository<Clients, Integer> {

}
