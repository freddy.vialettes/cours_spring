package fr.vialettes.freddy.spring.feature.database.category;

import jakarta.persistence.*;

public class CategoryDto {
    private Integer id;
    private String nom;

    public CategoryDto(Integer pId, String pNom) {
        id = pId;
        nom = pNom;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer pId) {
        id = pId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String pNom) {
        nom = pNom;
    }
}
