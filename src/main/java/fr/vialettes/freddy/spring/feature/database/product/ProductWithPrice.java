package fr.vialettes.freddy.spring.feature.database.product;

import jakarta.persistence.Column;
import jakarta.persistence.Tuple;

import java.math.BigDecimal;
import java.util.Objects;

public class ProductWithPrice extends ProductDto{

    @Column(name = "prix")
    private BigDecimal prix;

    public ProductWithPrice(Integer pId, String pNom, String pDescription, BigDecimal pPrix) {
        super(pId, pNom, pDescription);
        this.prix = pPrix;
    }

    public ProductWithPrice(Tuple pTuple) {
        super(pTuple);
        this.prix = BigDecimal.valueOf((Double) pTuple.get(3));
    }

    public BigDecimal getPrix() {
        return prix;
    }

    public void setPrix(BigDecimal pPrix) {
        prix = pPrix;
    }

    @Override
    public boolean equals(Object pO) {
        if (this == pO) return true;
        if (pO == null || getClass() != pO.getClass()) return false;
        if (!super.equals(pO)) return false;
        ProductWithPrice that = (ProductWithPrice) pO;
        return Objects.equals(prix, that.prix);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), prix);
    }
}
