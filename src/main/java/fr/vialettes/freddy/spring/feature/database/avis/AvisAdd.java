package fr.vialettes.freddy.spring.feature.database.avis;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AvisAdd {

    @JsonProperty(value = "description")
    private String description;

    @JsonProperty(value = "note")
    private Integer note;

    @JsonProperty(value = "produits_id")
    private Integer produits_id;

    @JsonProperty(value = "clients_id")
    private Integer clients_id;

    public AvisAdd(String pDescription, Integer pNote, Integer pProduits_id, Integer pClients_id) {
        description = pDescription;
        note = pNote;
        produits_id = pProduits_id;
        clients_id = pClients_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String pDescription) {
        description = pDescription;
    }

    public Integer getNote() {
        return note;
    }

    public void setNote(Integer pNote) {
        note = pNote;
    }

    public Integer getProduits_id() {
        return produits_id;
    }

    public void setProduits_id(Integer pProduits_id) {
        produits_id = pProduits_id;
    }

    public Integer getClients_id() {
        return clients_id;
    }

    public void setClients_id(Integer pClients_id) {
        clients_id = pClients_id;
    }
}
