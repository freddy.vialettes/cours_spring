package fr.vialettes.freddy.spring.feature.database.product;

import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;
import org.apache.tomcat.util.http.parser.HttpParser;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestValueException;
import org.springframework.web.bind.annotation.*;

import java.net.Inet4Address;
import java.util.*;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    private ProductService productService;
    private Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

    @GetMapping("")
    public ResponseEntity<?> getAllProduct() {
        try {
            List<Product> productList=  productService.getAllProduct();
            return ResponseEntity.ok(productList);
        }catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @PostMapping("")
    public ResponseEntity<?> insertNewProduct(@Valid @RequestBody Product pProduct) {
//        try {
//            return ResponseEntity.ok(productService.insertNewProduct(pProduct));
//        }catch (IllegalArgumentException pIllegalArgumentException) {
//            LOGGER.error(pIllegalArgumentException.getMessage());
//
//            Map<String, String> errors = new HashMap<>();
//            errors.put("name", pIllegalArgumentException.getMessage());
//
//            Map<String, Object> responseBody = new HashMap<>();
//            responseBody.put("errors", errors);
//
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseBody);
//        } catch (MissingRequestValueException pMissingRequestValueException) {
//            LOGGER.error(pMissingRequestValueException.getMessage());
//            Map<String, String> errors = new HashMap<>();
//            errors.put("test", pMissingRequestValueException.getMessage());
//
//            Map<String, Object> responseBody = new HashMap<>();
//            responseBody.put("errors", errors);
//
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseBody);
//        }
//        catch (MissingResourceException pMissingResourceException) {
//
//            Map<String, String> errors = new HashMap<>();
//            errors.put(pMissingResourceException.getKey(), pMissingResourceException.getMessage());
//
//            Map<String, Object> responseBody = new HashMap<>();
//            responseBody.put("errors", errors);
//
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseBody);
//        }

        return new ResponseEntity<>(pProduct, HttpStatus.OK);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, Object> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });

        Map<String, Object> response = new HashMap<>();
        response.put("errors", errors);
        return response;
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Product> updateProductName(@PathVariable Integer id, @RequestBody Product pProduct) {

        try {
            return ResponseEntity.ok(productService.updateProduct(id, pProduct));
        }catch (EntityNotFoundException pEntityNotFoundException) {
            LOGGER.error(pEntityNotFoundException.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        catch (IllegalArgumentException pIllegalArgumentException) {
            LOGGER.error(pIllegalArgumentException.getMessage());
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
    }

    @DeleteMapping("/{productId}/delete")
    public ResponseEntity<?> deleteProduct(@PathVariable Integer productId) {
        try {
            productService.deleteProduct(productId);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException pEntityNotFoundException) {
            LOGGER.error(pEntityNotFoundException.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
