package fr.vialettes.freddy.spring.feature;

import fr.vialettes.freddy.spring.feature.database.avis.AvisAdd;
import fr.vialettes.freddy.spring.feature.database.avis.AvisRepository;
import fr.vialettes.freddy.spring.feature.database.product.Product;
import fr.vialettes.freddy.spring.feature.database.product.ProductAdd;
import fr.vialettes.freddy.spring.feature.database.product.ProductDto;
import fr.vialettes.freddy.spring.feature.database.product.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/hello")
public class HelloController {

    private final Logger LOGGER = LoggerFactory.getLogger(HelloController.class);

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private AvisRepository avisRepository;

    @GetMapping(value = "/world", produces = MediaType.TEXT_PLAIN_VALUE)
    public String HelloWorld() {
        LOGGER.error("Méthode GET utilisée sur le point d'api /api/hello/world");
        return "Hello World !";
    }

    @PostMapping("/product")
    @ResponseStatus(HttpStatus.CREATED)
    public void addProduct(@RequestBody ProductAdd pProductAdd) {
        Product product = new Product();
        product.setId(pProductAdd.getId());
        product.setDescription(pProductAdd.getDescription());
        product.setNom(pProductAdd.getNom());
        product.setPrix(pProductAdd.getPrix());
        //product.setCategorie_id(pProductAdd.getCategorie_id());

        productRepository.save(product);
    }

    @DeleteMapping(value = "/product/{productId}/delete")
    public void deleteProduct (@PathVariable("productId") Integer productId) {
        productRepository.deleteById(productId);
    }

    @PostMapping(value = "/{productId}/avis")
    public void addAvis(@PathVariable("productId") Integer productId, @RequestBody AvisAdd avis) {

    }

    @PutMapping(value = "/{productId}/price")
    public void updateProductPrice(@PathVariable("productId") Integer productId, @RequestBody ProductAdd product) {

    }

    @DeleteMapping(value = "/{avisId}/delete")
    public void deleteAvis(@PathVariable("avisId") Integer avisId) {
        avisRepository.deleteById(avisId);
    }

}
